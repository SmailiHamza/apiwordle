package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class jdbcGameRepository implements GameRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public jdbcGameRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Game game) {
        jdbcTemplate.update(
                "insert into game values (?,?,?,?)",
                game.getTid(),
                game.getWordForSave(),
                game.getAttemptsLeft(),
                game.getUserTid().orElse(null));

    }

    public Optional<Game> findByTid(String tid) {
        try {
            String query = "select g.tid as tid,g.word as word,g.max_attemps as max_attemps,g.user_tid as user_tid, r.word as roundword from game g left join round r on g.tid=r.game_tid where g.tid=? order by r.round_order";
            Game game = jdbcTemplate.query(query,
                    new GameResultSetExtractor(), tid);
            return Optional.ofNullable(game);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public List<Game> findByUserTid(String userTid) {
        List<Game> listGame = new ArrayList<>();
        try {
            listGame = jdbcTemplate.query("select * from game where user_tid=?", new GameRowMapper(), userTid);
            return listGame;
        } catch (EmptyResultDataAccessException e) {
            return listGame;
        }
    }

    public Boolean userOwnsGame(String userTid, Game g) {
        try {
            jdbcTemplate.queryForObject("select * from game where tid=? and user_tid=?", new GameRowMapper(), g.getTid(), userTid);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    public void saveRound(Game game, String userInput) {
        jdbcTemplate.update(
                "insert into round values (?,?,?)",
                userInput,
                game.getRounds().size(),
                game.getTid());
    }

}
