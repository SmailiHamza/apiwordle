package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GameResultSetExtractor implements ResultSetExtractor<Game> {

    @Override
    public Game extractData(ResultSet rs) throws SQLException, DataAccessException {
        Game game=null;
        while (rs.next()){
            if(game==null){
                game=new Game(rs.getString("tid"),rs.getString("word"),rs.getInt("max_attemps"),rs.getString("user_tid"));
            }
            String word=rs.getString("roundword");

            if(word!=null){
                try {
                    if(game.getUserTid().isEmpty()){
                        game.guess(word);
                    }else{
                        game.guess(word,game.getUserTid().get());
                    }

                } catch (GameNotForThisUserException e) {
                    new GameNotForThisUserException();
                }
            }

        }
        System.out.println(game.getRounds().size());
        return game;
    }
}

