package com.zenika.academy.barbajavas.wordle.domain.model.game;

public enum GameState {
    WIN, LOSS, IN_PROGRESS
}
