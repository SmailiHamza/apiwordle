package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public interface GameRepository {

    public void save(Game game);
    
    public Optional<Game> findByTid(String tid);

    public List<Game> findByUserTid(String userTid) ;

    Boolean userOwnsGame(String userTid, Game g);
    public void saveRound(Game game,String userInput);
}
