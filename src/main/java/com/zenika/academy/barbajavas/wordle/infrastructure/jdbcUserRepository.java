package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class jdbcUserRepository implements UserRepository {
    private JdbcTemplate jdbcTemplate;
   @Autowired
   public  jdbcUserRepository(JdbcTemplate jdbcTemplate){
       this.jdbcTemplate=jdbcTemplate;
   }
    @Override
    public void save(User u) {
        try {
            jdbcTemplate.update(
                    "insert into users values (?,?,?)", u.getTid(), u.getEmail(), u.getUsername());
        }catch (DataIntegrityViolationException e){
           throw new IllegalArgumentException("Email already exist");
        }
    }

    @Override
    public void update(String name,String userTid) {
             jdbcTemplate.update(
                     "update users set name=? where tid=?",name,userTid);

    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            User u = jdbcTemplate.queryForObject("select * from users where email=?", new UserRowMapper(), email);
            return Optional.ofNullable(u);
        }catch (EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByTid(String userTid) {
        try {
            User u = jdbcTemplate.queryForObject("select * from users where tid=?", new UserRowMapper(), userTid);
            return Optional.ofNullable(u);
        }catch (EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public void delete(String userTid) {
        jdbcTemplate.update("delete from users where tid=?",userTid);
    }
}
