package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GameRowMapper implements RowMapper<Game> {

    @Override
    public Game mapRow(ResultSet rs, int rowNum) throws SQLException {
        Game game=new Game(rs.getString("tid"),rs.getString("word"),rs.getInt("max_attemps"),rs.getString("user_tid"));
        return game;
    }
}