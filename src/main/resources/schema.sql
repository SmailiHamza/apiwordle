create table if not exists users (
    tid char(36) primary key,
    email text not null unique,
    name text not null
);

create table if not exists game (
    tid char(36) primary key,
    word text not null,
    max_attemps int not null check (max_attemps > 0),
    user_tid char(36) references users(tid)
);

create table if not exists round (
    word text not null,
    round_order int not null,
    game_tid char(36) references game(tid) not null,
    primary key(round_order,game_tid)
);